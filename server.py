import asyncio
import aioredis
import json

from arena import Arena


class Server:
    def __init__(self):
        pass

    async def async_init(self, address: str, port: str, password: str):
        self.inp_redis = await aioredis.create_redis(
            address=(address, port), password=password
        )
        self.out_redis = await aioredis.create_redis(
            address=(address, port), password=password
        )
        temp = await self.inp_redis.subscribe("input")
        self.inp_chan = temp[0]
        self.out_chan = "output"
        self.arena = Arena()

    async def run(self):
        await asyncio.gather(self.read_input(), self.round_timer())

    def close(self):
        self.inp_redis.close()
        self.out_redis.close()

    async def read_input(self):
        async for message in self.inp_chan.iter(encoding="utf-8", decoder=json.loads):
            print(message)
            self.arena.set_action(**message)

    async def round_timer(self):
        while True:
            await asyncio.sleep(10)
            round_result = self.arena.round()
            alive_bullies = self.arena.status()
            await self.out_redis.publish_json(self.out_chan, round_result)
            await self.out_redis.publish_json(self.out_chan, alive_bullies)
