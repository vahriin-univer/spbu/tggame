from enum import Enum


class ActResult(Enum):
    HIT_SUCCESS = 0
    HIT_BESIDE = 1
    HIT_SLIP = 2
    HIT_TARGET_EVADE = 3
    EVADE_SUCCESS = 10
    EVADE_FOOL = 11
    STAND_SUCCESS = 20
    KILLED = 30

    def __int__(self):
        return self.value
