import random

from hit_case import HitCase
from actions import Action
from typing import Tuple


class Bully:
    def __init__(self, nick: str) -> None:
        self.nick = nick
        self.health = 100
        self.fatigue = 100

        self.round_action = Action.NOT_ON_ARENA
        self.target = ""
        self.hitted = False

    def hit(self) -> Tuple[HitCase, int]:
        """
        return HitCase status and number of damage
        """
        hit_prob = 50 + int(self.health / 2)
        # precision of hit depends of health
        if random.randint(0, 100) < hit_prob:
            # hit struck
            hit_damage = (
                5
                + random.randint(0, int(self.fatigue / 10))
                + random.randint(0, int(self.health / 5))
            )

            # random of hit beside
            if not random.randint(0, 100) < (10 + int((100 - self.fatigue) / 10)):
                # target hit
                return HitCase.HIT, hit_damage
            else:
                return HitCase.HIT_BESIDE, hit_damage

        else:
            # hit not struck
            return HitCase.SLIP, 0

    @property
    def evasion(self) -> bool:
        """
        return result of evasion
        """

        evasion_prob = 0
        if self.round_action is Action.EVASION:
            evasion_prob = 50 + random.randint(
                0, int((self.health + self.fatigue) / 10)
            )
        elif self.round_action is Action.STAND:
            evasion_prob = 0
        elif self.round_action is Action.NOT_ON_ARENA:
            evasion_prob = 100
        else:
            evasion_prob = 10

        return random.randint(0, 100) < evasion_prob

    def set_action(self, action_: Action, target: str = "") -> None:
        assert isinstance(action_, Action)
        if self.round_action is Action.NOT_ON_ARENA:
            return
        self.round_action = action_
        if target != "":
            self.target = target

    def decrease_health(self, value: int) -> bool:
        """return True if Bully be killed"""
        assert isinstance(value, int)
        self.health = max(0, self.health - value)
        if self.health == 0:
            self.round_action = Action.KILLED
            return True

        return False

    def increase_health(self, value: int) -> None:
        assert isinstance(value, int)
        self.health = min(self.health + value, 100)

    def round_end(self) -> None:
        if self.round_action is Action.KILLED:
            return
        elif self.round_action is Action.STAND:
            self.fatigue = min(self.fatigue + 50, 100)
        elif self.round_action is Action.EVASION:
            self.fatigue = max(self.fatigue - 25, 0)
        else:
            self.fatigue = max(0, self.fatigue - 10)

        self.round_action = Action.STAND
        self.target = ""
        self.hitted = False
